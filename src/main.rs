use clap::{App, Arg, SubCommand};

mod tags;

fn main() {
    let matches = App::new("Idol")
        .version("0.1.0-pre")
        .author("Leonardo Schwarz <mail@leoschwarz.com>")
        .subcommand(
            SubCommand::with_name("list")
                .about("List library elements")
                .alias("ls")
                .arg(
                    Arg::with_name("PATH-SPEC")
                        .help("Path specifier of the library to search.")
                        .index(1)
                        .required(false),
                ),
        )
        .subcommand(
            SubCommand::with_name("debug")
                .about("Various functions to be used during debugging but not for real users!")
                .subcommand(
                    SubCommand::with_name("read_tags")
                        .about("Read and display tags of a music file.")
                        .arg(
                            Arg::with_name("path")
                                .help("path of the sound file to read")
                                .index(1)
                                .required(true),
                        ),
                ),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("debug") {
        if let Some(matches) = matches.subcommand_matches("read_tags") {
            let path = matches.value_of("path").unwrap();
            if path.ends_with(".mp3") {
                tags::read_id3(path.into());
            } else if path.ends_with(".flac") {
                tags::read_flac(path.into());
            } else if path.ends_with(".ogg") {
                tags::read_ogg(path.into());
            }
        }
    }
}
