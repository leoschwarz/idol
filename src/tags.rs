// overview over v2.3 and v2.4 tags: https://en.wikipedia.org/wiki/ID3#ID3v2_frame_specification
// v2.3 tags: http://id3.org/id3v2.3.0
// v2.4 tags: http://id3.org/id3v2.4.0-frames

use std::collections::HashMap;
use std::fs::File;
use std::iter::FromIterator;

#[derive(Clone, Debug, Eq, PartialEq)]
enum Date {
    Combined(String),
    Separate(DateSeparate),
}

#[derive(Clone, Debug, Eq, PartialEq)]
struct DateSeparate {
    date: Option<String>,
    time: Option<String>,
    recording_dates: Option<String>,
    year: Option<String>,
}

fn make_separate_date(o: Option<Date>) -> DateSeparate {
    match o {
        Some(Date::Combined(_)) | None => DateSeparate {
            date: None,
            time: None,
            recording_dates: None,
            year: None,
        },
        Some(Date::Separate(s)) => s,
    }
}

// mp3
pub fn read_id3(path: String) {
    use id3::frame::Comment;
    use id3::{Tag, Version};

    let tag = Tag::read_from_path(path).unwrap();
    println!("tag: {:#?}", tag);

    /// Title/songname/content description
    let mut title: Option<String> = None;
    /// Album/Movie/Show title
    let mut album: Option<String> = None;
    /// Lead performer(s)/Soloist(s)
    let mut artist: Option<String> = None;
    let mut album_artist: Option<String> = None;

    let mut date = None;

    // TODO: Can there be multiple comments?
    let mut comments: Vec<Comment> = Vec::new();

    let mut track_num: Option<String> = None;
    let mut disk_num: Option<String> = None;

    let mut encoded_by: Option<String> = None;

    // TODO: TPE2,TPE3,TPE4?
    // TODO: Wrong content types should result in an Err().

    for frame in tag.frames() {
        match frame.id() {
            "TIT2" => {
                title = frame.content().text().map(String::from);
            }
            "TALB" => {
                album = frame.content().text().map(String::from);
            }
            "TPE1" => {
                artist = frame.content().text().map(String::from);
            }
            // This was actually never intended to store the album artist, but apparently is what
            // is most commonly used to tag so.
            "TPE2" => {
                album_artist = frame.content().text().map(String::from);
            }
            "COMM" => {
                if let Some(comm) = frame.content().comment() {
                    comments.push(comm.clone());
                }
            }
            "TRCK" => {
                track_num = frame.content().text().map(String::from);
            }
            "TPOS" => {
                disk_num = frame.content().text().map(String::from);
            }
            "TDRC" => {
                date = frame.content().text().map(|s| Date::Combined(s.into()));
            }
            "TYER" => {
                let mut d = make_separate_date(date);
                d.year = frame.content().text().map(String::from);
                date = Some(Date::Separate(d));
            }
            "TIME" => {
                let mut d = make_separate_date(date);
                d.time = frame.content().text().map(String::from);
                date = Some(Date::Separate(d));
            }
            "TRDA" => {
                let mut d = make_separate_date(date);
                d.recording_dates = frame.content().text().map(String::from);
                date = Some(Date::Separate(d));
            }
            "TDAT" => {
                let mut d = make_separate_date(date);
                d.date = frame.content().text().map(String::from);
                date = Some(Date::Separate(d));
            }
            "TENC" => {
                encoded_by = frame.content().text().map(String::from);
            }
            _ => {}
        }
    }

    println!("title: {:?}", title);
    println!("album: {:?}", album);
    println!("artist: {:?}", artist);
    println!("album_artist: {:?}", album_artist);
    println!("date: {:?}", date);
    println!("comments: {:?}", comments);
    println!("track_num: {:?}", track_num);
    println!("track_disk: {:?}", disk_num);
    println!("encoded_by: {:?}", encoded_by);
}

fn read_vorbis_comments(comments: HashMap<String, String>) {
    let title = comments.get("TITLE");
    let album = comments.get("ALBUM");
    let artist = comments.get("ARTIST");
    let album_artist = comments.get("ALBUMARTIST");
    let date = comments.get("DATE");
    // comments?
    let track_num = comments.get("TRACKNUMBER");
    let disk_num = comments.get("DISCNUMBER");
    // encoded by
    let total_tracks = comments.get("TOTALTRACKS");
    let total_disks = comments.get("TOTALDISCS");

    println!("title: {:?}", title);
    println!("album: {:?}", album);
    println!("artist: {:?}", artist);
    println!("album artist: {:?}", album_artist);
    println!("date: {:?}", date);
    //println!("comments: {:?}", comments);
    println!("track_num (total): {:?} ({:?})", track_num, total_tracks);
    println!("disk_num  (total): {:?} ({:?})", disk_num, total_disks);
    //println!("encoded_by: {:?}", encoded_by);
}

pub fn read_flac(path: String) {
    let vorbis_comment = flac::metadata::get_vorbis_comment(path.as_str()).unwrap();
    let comments = vorbis_comment.comments;
    println!("comments: {:?}", comments);
    read_vorbis_comments(comments)
}

// TODO: Currently does not work with some of my files.
pub fn read_ogg(path: String) {
    let mut file = File::open(path).unwrap();
    let reader = lewton::inside_ogg::OggStreamReader::new(&mut file).unwrap();
    let comment_header = reader.comment_hdr;
    let encoder = comment_header.vendor;
    println!("encoder: {:?}", encoder);

    // TODO: avoid this in the future
    let mut comments_map = HashMap::new();
    for (k, v) in comment_header.comment_list {
        comments_map.insert(k, v);
    }
    read_vorbis_comments(comments_map);
}
